resource "aws_instance" "ambiente-producao" {
  ami           = "ami-04b70fa74e45c3917"
  instance_type = "t2.micro"
  key_name      = "Projeto-Phoebus"
  
  vpc_security_group_ids = [
    aws_security_group.allow_ssh.id,
    aws_security_group.allow_http.id,
    aws_security_group.allow_egress.id,
  ]

  user_data = file("script.sh")

  tags = {
    Name = "ambiente-producao"
  }
}
